package ua.nure.dtpcontrol.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.EditText;

import java.util.regex.Pattern;

import ua.nure.dtpcontrol.R;


public class Verification {

    public static boolean verifyName(Context context, EditText input) {
        String name = input.getText().toString();
        Pattern NAME_PATTERN = Pattern.compile("^([\\w]{3,})+\\s+([\\w\\s]{3,})+$");
        if (!TextUtils.isEmpty(name) && NAME_PATTERN.matcher(name).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.name_tip));
            return false;
        }
    }

    public static boolean verifyEmail(Context context, EditText input) {
        String email = input.getText().toString();
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        if (pattern.matcher(email).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.email_tip));
            return false;
        }
    }

    public static boolean verifyDOB(Context context, EditText input) {
        String dob = input.getText().toString();
        Pattern DOB_PATTERN = Pattern.compile("^(0?[1-9]|1[0-2])\\.([0-2]?[1-9]|[1-3][01])\\.\\d{4}$");
        if (!TextUtils.isEmpty(dob) && DOB_PATTERN.matcher(dob).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.dob_tip));
            return false;
        }
    }

    public static boolean verifyPassport(Context context, EditText input) {
        String passport = input.getText().toString();
        Pattern PASSPORT_PATTERN = Pattern.compile("^[a-zA-Z]{2}\\d{6}$");
        if (!TextUtils.isEmpty(passport) && PASSPORT_PATTERN.matcher(passport).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.passport_tip));
            return false;
        }
    }
    public static boolean verifyCode(Context context, EditText input) {
        String code = input.getText().toString();
        Pattern CODE_PATTERN = Pattern.compile("[0-9]{10}");
        if (!TextUtils.isEmpty(code) && CODE_PATTERN.matcher(code).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.idcode_tip));
            return false;
        }
    }

    public static boolean verifyBrand(Context context, EditText input) {
        String brand = input.getText().toString();
        Pattern BRAND_PATTERN = Pattern.compile("^[a-zA-ZА-Яа-я]+$");
        if (!TextUtils.isEmpty(brand) && BRAND_PATTERN.matcher(brand).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.brand_tip));
            return false;
        }
    }
    public static boolean verifyYear(Context context, EditText input) {
        String year = input.getText().toString();
        Pattern YEAR_PATTERN = Pattern.compile("^\\d{4}$");
        if (!TextUtils.isEmpty(year) && YEAR_PATTERN.matcher(year).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.year_tip));
            return false;
        }
    }
    public static boolean verifyCost(Context context, EditText input) {
        String cost = input.getText().toString();
        Pattern COST_PATTERN = Pattern.compile("^[0-9]+$");
        if (!TextUtils.isEmpty(cost) && COST_PATTERN.matcher(cost).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.cost_tip));
            return false;
        }
    }
    public static boolean verifyNumber(Context context, EditText input) {
        String number = input.getText().toString();
        Pattern NUMBER_PATTERN = Pattern.compile("^[А-Я]{2}[0-9]{4}[А-Я]{2}$");
        if (!TextUtils.isEmpty(number) && NUMBER_PATTERN.matcher(number).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.number_tip));
            return false;
        }
    }

    public static boolean verifyPassword(Context context, EditText input) {
        String password = input.getText().toString();
        Pattern PASSWORD_PATTERN = Pattern.compile("[a-zA-Z0-9]{8,24}");
        if (!TextUtils.isEmpty(password) && PASSWORD_PATTERN.matcher(password).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.password_tip));
            return false;
        }
    }

    public static boolean verifyCarNumbers(Context context, EditText input) {
        String carNum = input.getText().toString();
        if (!TextUtils.isEmpty(carNum)) {
            return true;
        } else {
            input.setError(context.getString(R.string.cars_num_tip));
            return false;
        }
    }

    public static boolean verifyNumberOfCars(Context context, EditText input) {
        String numOfCar = input.getText().toString();
        Pattern NUM_OF_CARS_PATTERN = Pattern.compile("^[0-9]+$");
        if (!TextUtils.isEmpty(numOfCar) && NUM_OF_CARS_PATTERN.matcher(numOfCar).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.numofcars_tip));
            return false;
        }
    }

    public static boolean verifyAddress(Context context, EditText input) {
        String address = input.getText().toString();
        if (!TextUtils.isEmpty(address)) {
            return true;
        } else {
            input.setError(context.getString(R.string.address_tip));
            return false;
        }
    }
}
