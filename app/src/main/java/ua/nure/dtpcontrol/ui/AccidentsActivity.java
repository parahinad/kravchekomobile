package ua.nure.dtpcontrol.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.dtpcontrol.R;
import ua.nure.dtpcontrol.data.Accident;
import ua.nure.dtpcontrol.data.Accidents;
import ua.nure.dtpcontrol.data.User;
import ua.nure.dtpcontrol.retrofit.JSONPlaceHolderApi;
import ua.nure.dtpcontrol.retrofit.RetrofitService;
import ua.nure.dtpcontrol.ui.rva.AccidentsRVA;

public class AccidentsActivity extends AppCompatActivity {

    private static final String TAG = "AccidentsActivity";

    private List<Accident> mAccidents;
    private RecyclerView mRecyclerView;
    private JSONPlaceHolderApi mApi;
    private ImageButton mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accidents);

        mRecyclerView = (RecyclerView) findViewById(R.id.accidents_rv);
        mBackButton = findViewById(R.id.back_btn);

        mApi = RetrofitService.getInstance().getApiService();

        LinearLayoutManager llm = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setHasFixedSize(true);

        mBackButton.setOnClickListener((v) -> {
            navigateToMenuScreen();
            finish();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeData();
    }

    private void initializeData(){
        mAccidents = new ArrayList<>();
        mApi.getAccidents(User.getInstance().getId()).enqueue(smartDeviceCallback);
    }


    Callback<Accidents> smartDeviceCallback = new Callback<Accidents>() {
        @Override
        public void onResponse(Call<Accidents> call, Response<Accidents> response) {
            if(!response.isSuccessful()) {
                System.out.println(response.code());
                return;
            }
            List<Accident> mAccidentList = response.body().getAccidents();
            for (Accident accident : mAccidentList) {
                mAccidents.add(new Accident(accident.getId(), accident.getIdUser(), accident.getAddress(), accident.getQuantityOfCars(), accident.getBeatingDegree(), accident.getCarsNumbers(), accident.getStatus(), accident.getDate()));
            }
            initializeAdapter();
        }

        @Override
        public void onFailure(Call<Accidents> call, Throwable t) {
            System.out.println(t);
            Log.i(TAG, t.getMessage());
        }
    };

    private void initializeAdapter(){
        AccidentsRVA adapter = new AccidentsRVA(this, mAccidents);
        mRecyclerView.setAdapter(adapter);
    }

    private void navigateToMenuScreen() {
        Intent intent = new Intent(AccidentsActivity.this,
                MenuActivity.class);
        startActivity(intent);
    }
}