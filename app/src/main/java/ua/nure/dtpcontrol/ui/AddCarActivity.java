package ua.nure.dtpcontrol.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.dtpcontrol.R;
import ua.nure.dtpcontrol.data.Car;
import ua.nure.dtpcontrol.data.User;
import ua.nure.dtpcontrol.retrofit.JSONPlaceHolderApi;
import ua.nure.dtpcontrol.retrofit.RetrofitService;
import ua.nure.dtpcontrol.util.InternetConnection;
import ua.nure.dtpcontrol.util.Verification;

public class AddCarActivity extends AppCompatActivity {

    private static final String TAG = "AddCarActivity";
    Button mCancelButton, mSaveButton;
    Car mCar;
    EditText mBrandET, mNumberET, mYearET, mCostET;
    private JSONPlaceHolderApi mApi;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_car);

        mCancelButton = findViewById(R.id.cancel_add_cars_btn);
        mSaveButton = findViewById(R.id.save_add_cars_btn);
        mBrandET = findViewById(R.id.add_brand_input);
        mBrandET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyBrand(this, mBrandET);
            }
        });
        mNumberET = findViewById(R.id.add_number_input);
        mNumberET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyNumber(this, mNumberET);
            }
        });
        mYearET = findViewById(R.id.add_year_input);
        mYearET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyYear(this, mYearET);
            }
        });
        mCostET = findViewById(R.id.add_cost_input);
        mCostET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyCost(this, mCostET);
            }
        });
        mCar = new Car();

        token = "Bearer " + User.getInstance().getToken();

        mApi = RetrofitService.getInstance().getApiService();

        mCancelButton.setOnClickListener(v -> {
            finish();
        });

        mSaveButton.setOnClickListener(v -> {
            addService();
        });
    }

    private void addService() {
        if (!Verification.verifyBrand(this, mBrandET)
                || !Verification.verifyNumber(this, mNumberET)
                || !Verification.verifyCost(this, mCostET)
                || !Verification.verifyYear(this, mYearET)) {
            return;
        } else if (!InternetConnection.checkConnection(getApplicationContext())) {
            Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_LONG).show();
            return;
        } else {
            mCar.setBrand(mBrandET.getText().toString());
            mCar.setNumber(mNumberET.getText().toString());
            mCar.setYear(Integer.parseInt(mYearET.getText().toString()));
            mCar.setCost(Integer.parseInt(mCostET.getText().toString()));
            mCar.setIdUser(Integer.parseInt(User.getInstance().getId()));

            mApi.addCar(token, mCar).enqueue(addCarCallback);
            Intent intent = new Intent(AddCarActivity.this, MenuActivity.class);
            startActivity(intent);
        }
    }

    Callback<Car> addCarCallback = new Callback<Car>() {
        @Override
        public void onResponse(Call<Car> call, Response<Car> response) {
            if (response.isSuccessful()) {
                System.out.println(response.body());
                finish();
            }
        }

        @Override
        public void onFailure(Call<Car> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };
}

