package ua.nure.dtpcontrol.ui;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.dtpcontrol.R;
import ua.nure.dtpcontrol.data.Report;
import ua.nure.dtpcontrol.data.User;
import ua.nure.dtpcontrol.retrofit.JSONPlaceHolderApi;
import ua.nure.dtpcontrol.retrofit.RetrofitService;
import ua.nure.dtpcontrol.util.InternetConnection;
import ua.nure.dtpcontrol.util.Verification;

public class AddRequestActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener{

    private static final String TAG = "AddRequestActivity";
    Button mCancelButton, mSaveButton;
    Report mReport;
    EditText mAddressET, mNumberOfCarsET, mNumbersET;
    TextView mCurRate;
    SeekBar mRateSeekBar;
    private JSONPlaceHolderApi mApi;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_request);

        mCurRate = findViewById(R.id.start_tv);
        mCancelButton = findViewById(R.id.cancel_add_req_btn);
        mSaveButton = findViewById(R.id.save_add_req_btn);
        mAddressET = findViewById(R.id.add_address_input);
        mAddressET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyAddress(this, mAddressET);
            }
        });
        mNumberOfCarsET = findViewById(R.id.add_q_cars_input);
        mNumberOfCarsET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyNumberOfCars(this, mNumberOfCarsET);
            }
        });
        mNumbersET = findViewById(R.id.add_cars_numbers_input);
        mNumbersET.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyCarNumbers(this, mNumbersET);
            }
        });

        mRateSeekBar = findViewById(R.id.add_degree_bar);
        mRateSeekBar.setOnSeekBarChangeListener(this);

        token = "Bearer " + User.getInstance().getToken();

        mApi = RetrofitService.getInstance().getApiService();

        mCancelButton.setOnClickListener(v -> {
            finish();
        });

        mSaveButton.setOnClickListener(v -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                addService();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void addService() {
        if (!Verification.verifyAddress(this, mAddressET)
                || !Verification.verifyNumberOfCars(this, mNumberOfCarsET)
                || !Verification.verifyCarNumbers(this, mNumbersET)){
            return;
        } else if (!InternetConnection.checkConnection(getApplicationContext())) {
            Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_LONG).show();
            return;
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

            mReport = new Report(mAddressET.getText().toString(), Integer.parseInt(mNumberOfCarsET.getText().toString()), mRateSeekBar.getProgress(), mNumbersET.getText().toString(), dateFormat.format(new Date()));

            mApi.addReport(token, mReport).enqueue(addReportCallback);
            Intent intent = new Intent(AddRequestActivity.this, MenuActivity.class);
            startActivity(intent);
        }
    }

    Callback<Report> addReportCallback = new Callback<Report>() {
        @Override
        public void onResponse(Call<Report> call, Response<Report> response) {
            if (response.isSuccessful()) {
                System.out.println(response.body());
                finish();
            }
        }

        @Override
        public void onFailure(Call<Report> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        mCurRate.setText(String.valueOf(seekBar.getProgress()));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mCurRate.setText(String.valueOf(seekBar.getProgress()));
    }
}
