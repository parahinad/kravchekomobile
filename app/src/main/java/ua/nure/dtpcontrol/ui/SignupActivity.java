package ua.nure.dtpcontrol.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.dtpcontrol.R;
import ua.nure.dtpcontrol.data.User;
import ua.nure.dtpcontrol.retrofit.RetrofitService;
import ua.nure.dtpcontrol.util.InternetConnection;
import ua.nure.dtpcontrol.util.Verification;

public class SignupActivity extends AppCompatActivity {

    private static final String TAG = "SignUpActivity";

    EditText mName, mEmail, mPassword, mDOB, mPassport, mCode;
    Button mConfirm;
    LinearLayout mGoToLogin;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signup);

        context = this;

        init();
    }

    private void init() {
        mName = findViewById(R.id.fullname);
        mName.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyName(this, mName);
            }
        });

        mDOB = findViewById(R.id.dob);
        mDOB.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyDOB(this, mDOB);
            }
        });

        mPassport = findViewById(R.id.passport);
        mPassport.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyPassport(this, mPassport);
            }
        });

        mCode = findViewById(R.id.idcode);
        mCode.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyCode(this, mCode);
            }
        });


        mEmail = findViewById(R.id.email);
        mEmail.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyEmail(this, mEmail);
            }
        });

        mPassword = findViewById(R.id.password);
        mPassword.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyPassword(this, mPassword);
            }
        });

        mConfirm = findViewById(R.id.signup_button);
        mConfirm.setOnClickListener(v -> signUp(
                mName.getText().toString(),
                mEmail.getText().toString(),
                mPassport.getText().toString(),
                mDOB.getText().toString(),
                mCode.getText().toString(),
                mPassword.getText().toString()
        ));

        mGoToLogin = findViewById(R.id.go_to_signin);
        mGoToLogin.setOnClickListener(v -> {
            Intent intent = new Intent(SignupActivity.this, SigninActivity.class);
            startActivity(intent);
        });
    }

    private void signUp(String name, String email, String passport, String dob, String code, String password) {
        if (!Verification.verifyName(this, mName)
                || !Verification.verifyEmail(this, mEmail)
                || !Verification.verifyCode(this, mCode)
                || !Verification.verifyDOB(this, mDOB)
                || !Verification.verifyPassport(this, mPassport)
                || !Verification.verifyPassword(this, mPassword)) {
            return;
        } else if (!InternetConnection.checkConnection(getApplicationContext())) {
            Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_LONG).show();
            return;
        } else {
            User user = User.getInstance()
                    .setFullName(name)
                    .setEmail(email)
                    .setPassword(password)
                    .setDateOfBirth(dob)
                    .setIdentificationCode(code)
                    .setNumberPasport(passport)
                    .setBalance(0);

            RetrofitService.getInstance()
                    .getApiService()
                    .userSignup(user)
                    .enqueue(signupCallback);
        }
    }

    private Callback<User> signupCallback = new Callback<User>() {
        @Override
        public void onResponse(Call<User> call, Response<User> response) {
            if(!response.isSuccessful()) {
                Log.i(TAG, response.message() + " " + response.code());
                Toast.makeText(
                        SignupActivity.this,
                        response.message(),
                        Toast.LENGTH_SHORT
                ).show();
            } else {
                Intent intent = new Intent(SignupActivity.this,
                        SigninActivity.class);
                startActivity(intent);
            }
        }

        @Override
        public void onFailure(Call<User> call, Throwable t) {
            Log.i(TAG, t.getMessage());
            Toast.makeText(
                    SignupActivity.this,
                    t.getMessage(),
                    Toast.LENGTH_SHORT
            ).show();
        }
    };
}

