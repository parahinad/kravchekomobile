package ua.nure.dtpcontrol.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.dtpcontrol.R;
import ua.nure.dtpcontrol.data.User;
import ua.nure.dtpcontrol.retrofit.RetrofitService;
import ua.nure.dtpcontrol.util.InternetConnection;
import ua.nure.dtpcontrol.util.Verification;

public class SigninActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    EditText mEmail, mPassword;
    Button mConfirm;
    LinearLayout mGoToLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signin);

        init();
    }

    private void init() {

        mEmail = findViewById(R.id.email);
        mEmail.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyEmail(this, mEmail);
            }
        });

        mPassword = findViewById(R.id.password);
        mPassword.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                Verification.verifyPassword(this, mPassword);
            }
        });

        mConfirm = findViewById(R.id.signup_button);
        mConfirm.setOnClickListener(v -> signIn(
                mEmail.getText().toString(),
                mPassword.getText().toString()
        ));

        mGoToLogin = findViewById(R.id.go_to_signin);
        mGoToLogin.setOnClickListener(v -> {
            Intent intent = new Intent(SigninActivity.this, SignupActivity.class);
            startActivity(intent);
        });
    }

    private void signIn(String email, String password) {
        if (!Verification.verifyEmail(this, mEmail)
                || !Verification.verifyPassword(this, mPassword)) {
            return;
        } else if (!InternetConnection.checkConnection(getApplicationContext())) {
            Toast.makeText(this, R.string.no_internet_connection, Toast.LENGTH_LONG).show();
            return;
        } else {
            User user = User.getInstance()
                    .setEmail(email)
                    .setPassword(password);

            RetrofitService.getInstance()
                    .getApiService()
                    .userLogin(user)
                    .enqueue(loginCallback);
        }
    }

    private Callback<User> loginCallback = new Callback<User>() {
        @Override
        public void onResponse(Call<User> call, Response<User> response) {
            if(!response.isSuccessful()) {
                Log.i(TAG, response.message());
                Toast.makeText(
                        SigninActivity.this,
                        response.message(),
                        Toast.LENGTH_SHORT
                ).show();
            } else {
                User.getInstance()
                        .setToken(response.body().getToken())
                        .setId(response.body().getId());

                Intent intent = new Intent(SigninActivity.this,
                        MenuActivity.class);
                startActivity(intent);
            }
        }

        @Override
        public void onFailure(Call<User> call, Throwable t) {
            Log.i(TAG, t.toString());
            Toast.makeText(
                    SigninActivity.this,
                    t.getMessage(),
                    Toast.LENGTH_SHORT
            ).show();
        }
    };
}