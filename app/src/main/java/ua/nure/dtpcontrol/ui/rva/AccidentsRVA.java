package ua.nure.dtpcontrol.ui.rva;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Locale;

import ua.nure.dtpcontrol.R;
import ua.nure.dtpcontrol.data.Accident;

public class AccidentsRVA extends RecyclerView.Adapter<AccidentsRVA.AccidentsViewHolder>{

    private static final String TAG = "DevicesRVA";

    private Context mContext;

    public AccidentsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.accident_card, viewGroup, false);

        AccidentsViewHolder smartDevicesViewHolder = new AccidentsViewHolder(v);
        return smartDevicesViewHolder;
    }

    private List<Accident> mAccidents;

    public AccidentsRVA(Context context, List<Accident> accidentList){
        this.mContext = context;
        this.mAccidents = accidentList;
    }

    @Override
    public void onBindViewHolder(@NonNull AccidentsViewHolder holder, int position) {
        holder.mCardView.setId(mAccidents.get(position).getId());
        holder.mDeviceNameTV.setText(mAccidents.get(position).getAddress());
        holder.mResourceNameTV.setText(String.format(Locale.getDefault(), "%d", mAccidents.get(position).getBeatingDegree()));
        holder.mResourceAmountTV.setText(mAccidents.get(position).getCarsNumbers());
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return mAccidents.size();
    }

    class AccidentsViewHolder extends RecyclerView.ViewHolder {
        CardView mCardView;
        TextView mDeviceNameTV, mResourceNameTV, mResourceAmountTV, mResAddedTV;
        Button mAddResourceButton;

        AccidentsViewHolder(View itemView) {
            super(itemView);
            mCardView = (CardView) itemView.findViewById(R.id.accident_cv);
            mDeviceNameTV = (TextView) itemView.findViewById(R.id.name_device_label);
            mResourceAmountTV = (TextView) itemView.findViewById(R.id.resource_amount);
            mResourceNameTV = (TextView) itemView.findViewById(R.id.resource_label);
        }

    }

}


