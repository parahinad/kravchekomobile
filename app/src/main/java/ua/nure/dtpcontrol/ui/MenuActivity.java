package ua.nure.dtpcontrol.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.dtpcontrol.R;
import ua.nure.dtpcontrol.data.User;
import ua.nure.dtpcontrol.data.Users;
import ua.nure.dtpcontrol.retrofit.RetrofitService;



public class MenuActivity extends AppCompatActivity {
    private static final String TAG = "MenuActivity";
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        token = "Bearer " + User.getInstance().getToken();
        initializeData();
        init();
    }

    private void init() {
        findViewById(R.id.profile_btn).setOnClickListener(v -> goTo(ProfileActivity.class));
        findViewById(R.id.add_car_btn).setOnClickListener(v -> goTo(AddCarActivity.class));
        findViewById(R.id.create_request_btn).setOnClickListener(v -> goTo(AddRequestActivity.class));
    }

    private void goTo(Class cls) {
        Intent intent = new Intent(MenuActivity.this, cls);
        startActivity(intent);
    }

    private void initializeData(){
        RetrofitService.getInstance()
                .getApiService()
                .getUser(token)
                .enqueue(getUserDataCallback);
    }
    Callback<Users> getUserDataCallback = new Callback<Users>() {
        @Override
        public void onResponse(Call<Users> call, Response<Users> response) {
            if(!response.isSuccessful()) {
                System.out.println(response.code());
                return;
            }
            User user = response.body().getUser();
            User.getInstance()
                    .setDateOfBirth(user.getDateOfBirth())
                    .setIdentificationCode(user.getIdentificationCode())
                    .setNumberPasport(user.getNumberPasport())
                    .setFullName(user.getFullName())
                    .setEmail(user.getEmail())
                    .setBalance(user.getBalance())
                    .setPassword(user.getPassword());
        }

        @Override
        public void onFailure(Call<Users> call, Throwable t) {
            System.out.println(t);
            Log.i(TAG, t.getMessage());
        }
    };
}