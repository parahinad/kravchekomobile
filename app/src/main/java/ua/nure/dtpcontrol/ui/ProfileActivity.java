package ua.nure.dtpcontrol.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.nure.dtpcontrol.R;
import ua.nure.dtpcontrol.data.User;
import ua.nure.dtpcontrol.retrofit.RetrofitService;

public class ProfileActivity extends AppCompatActivity {

    TextView mNameTV, mEmailTV, mPassportTV, mDoBTV, mCodeTV, mBalanceTV;
    EditText mNameET, mEmailET, mPassportET, mDoBET, mCodeET;
    Button mEditButton;

    ImageView mBack, mLogOut;

    Context context;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();
    }

    private void init() {
        context = this;

        mNameTV = findViewById(R.id.profile_name);
        mEmailTV = findViewById(R.id.profile_email);
        mPassportTV = findViewById(R.id.profile_passport);
        mDoBTV = findViewById(R.id.profile_dob);
        mCodeTV = findViewById(R.id.profile_code);
        mBalanceTV = findViewById(R.id.profile_balance);

        mNameET = findViewById(R.id.profile_name_edit);
        mEmailET = findViewById(R.id.profile_email_edit);
        mPassportET = findViewById(R.id.profile_passport_edit);
        mDoBET = findViewById(R.id.profile_dob_edit);
        mCodeET = findViewById(R.id.profile_code_edit);

        mEditButton = findViewById(R.id.edit_profile_btn);
        mEditButton.setOnClickListener(v -> startEditing());

        mBack = findViewById(R.id.back_btn);
        mBack.setOnClickListener(v -> finish());

        mLogOut = findViewById(R.id.exit_btn);
        mLogOut.setOnClickListener(v -> {
            User.getInstance().setToken(null);
            startActivity(new Intent(ProfileActivity.this, SigninActivity.class));
        });


        showTV();
    }

    private void showTV() {
        mEditButton.setText(R.string.edit);

        mNameET.setVisibility(View.GONE);
        mEmailET.setVisibility(View.GONE);
        mPassportET.setVisibility(View.GONE);
        mDoBET.setVisibility(View.GONE);
        mCodeET.setVisibility(View.GONE);
        mNameTV.setVisibility(View.VISIBLE);
        mEmailTV.setVisibility(View.VISIBLE);
        mPassportTV.setVisibility(View.VISIBLE);
        mDoBTV.setVisibility(View.VISIBLE);
        mCodeTV.setVisibility(View.VISIBLE);

        mNameTV.setText(User.getInstance().getFullName());
        mEmailTV.setText(User.getInstance().getEmail());
        mPassportTV.setText(User.getInstance().getNumberPasport());
        mDoBTV.setText(User.getInstance().getDateOfBirth());
        mCodeTV.setText(User.getInstance().getIdentificationCode());
        mBalanceTV.setText(String.format(Locale.getDefault(), "%d", User.getInstance().getBalance()));
    }

    private void showET() {
        mEditButton.setText(R.string.save);

        mNameTV.setVisibility(View.GONE);
        mEmailTV.setVisibility(View.GONE);
        mPassportTV.setVisibility(View.GONE);
        mCodeTV.setVisibility(View.GONE);
        mDoBTV.setVisibility(View.GONE);
        mNameET.setVisibility(View.VISIBLE);
        mEmailET.setVisibility(View.VISIBLE);
        mPassportET.setVisibility(View.VISIBLE);
        mDoBET.setVisibility(View.VISIBLE);
        mCodeET.setVisibility(View.VISIBLE);

        mNameET.setText(User.getInstance().getFullName());
        mEmailET.setText(User.getInstance().getEmail());
        mPassportET.setText(User.getInstance().getNumberPasport());
        mDoBET.setText(User.getInstance().getDateOfBirth());
        mCodeET.setText(User.getInstance().getIdentificationCode());

    }


    private void startEditing() {
        showET();
        mEditButton.setOnClickListener(v -> submitEditing());
    }

    private void submitEditing() {
        Callback<Void> callback = new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(context, response.message(), Toast.LENGTH_LONG).show();
                } else {
                    mEditButton.setOnClickListener(v -> startEditing());
                    showTV();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        };

        User.getInstance().setFullName(mNameET.getText().toString())
                .setEmail(mEmailET.getText().toString())
                .setNumberPasport(mPassportET.getText().toString())
                .setIdentificationCode(mCodeET.getText().toString())
                .setDateOfBirth(mDoBET.getText().toString());

        RetrofitService.getInstance()
                .getApiService()
                .updateUser(token, User.getInstance())
                .enqueue(callback);
    }
}