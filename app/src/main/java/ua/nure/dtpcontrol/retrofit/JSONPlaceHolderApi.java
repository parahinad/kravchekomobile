package ua.nure.dtpcontrol.retrofit;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import ua.nure.dtpcontrol.data.Accidents;
import ua.nure.dtpcontrol.data.Car;
import ua.nure.dtpcontrol.data.Report;
import ua.nure.dtpcontrol.data.User;
import ua.nure.dtpcontrol.data.Users;

public interface JSONPlaceHolderApi {

    @POST("/user/login")
    Call<User> userLogin(@Body User user);

    @POST("/user/signup")
    Call<User> userSignup(@Body User user);

    @POST("/user/update")
    Call<Void> updateUser(@Header("Authorization") String token,
                                @Body User user);

    @GET("/user/getProfile")
    Call<Users> getUser(@Header("Authorization") String token);

    @POST("/car/create")
    Call<Car> addCar(@Header("Authorization") String token,
                     @Body Car car);
    @POST("/user/createAccident")
    Call<Report> addReport(@Header("Authorization") String token,
                     @Body Report report);

    @GET("/accident")
    Call<Accidents> getAccidents(@Header("Authorization") String token);

}
