package ua.nure.dtpcontrol.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {
    private static RetrofitService mInstance;
    public static String ROOT_URL = "https://dtpcontrol.herokuapp.com/";
    private Retrofit mRetrofit;

    private RetrofitService() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static RetrofitService getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitService();
        }
        return mInstance;
    }

    public JSONPlaceHolderApi getApiService() {
        return mRetrofit.create(JSONPlaceHolderApi.class);
    }
}