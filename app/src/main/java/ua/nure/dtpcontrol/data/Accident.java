package ua.nure.dtpcontrol.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Accident {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("idUser")
    @Expose
    private Integer idUser;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("quantityOfCars")
    @Expose
    private Integer quantityOfCars;
    @SerializedName("beatingDegree")
    @Expose
    private Integer beatingDegree;
    @SerializedName("carsNumbers")
    @Expose
    private String carsNumbers;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("date")
    @Expose
    private String date;

    public Accident(Integer id, Integer idUser, String address, Integer quantityOfCars, Integer beatingDegree, String carsNumbers, String status, String date) {
        this.id = id;
        this.idUser = idUser;
        this.address = address;
        this.quantityOfCars = quantityOfCars;
        this.beatingDegree = beatingDegree;
        this.carsNumbers = carsNumbers;
        this.status = status;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getQuantityOfCars() {
        return quantityOfCars;
    }

    public void setQuantityOfCars(Integer quantityOfCars) {
        this.quantityOfCars = quantityOfCars;
    }

    public Integer getBeatingDegree() {
        return beatingDegree;
    }

    public void setBeatingDegree(Integer beatingDegree) {
        this.beatingDegree = beatingDegree;
    }

    public String getCarsNumbers() {
        return carsNumbers;
    }

    public void setCarsNumbers(String carsNumbers) {
        this.carsNumbers = carsNumbers;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}