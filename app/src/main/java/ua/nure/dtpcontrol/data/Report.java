package ua.nure.dtpcontrol.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Report {
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("quantityOfCars")
    @Expose
    private Integer quantityOfCars;
    @SerializedName("beatingDegree")
    @Expose
    private Integer beatingDegree;
    @SerializedName("carsNumbers")
    @Expose
    private String carsNumbers;
    @SerializedName("date")
    @Expose
    private String date;

    public Report(String address, Integer quantityOfCars, Integer beatingDegree, String carsNumbers, String date) {
        this.address = address;
        this.quantityOfCars = quantityOfCars;
        this.beatingDegree = beatingDegree;
        this.carsNumbers = carsNumbers;
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getQuantityOfCars() {
        return quantityOfCars;
    }

    public void setQuantityOfCars(Integer quantityOfCars) {
        this.quantityOfCars = quantityOfCars;
    }

    public Integer getBeatingDegree() {
        return beatingDegree;
    }

    public void setBeatingDegree(Integer beatingDegree) {
        this.beatingDegree = beatingDegree;
    }

    public String getCarsNumbers() {
        return carsNumbers;
    }

    public void setCarsNumbers(String carsNumbers) {
        this.carsNumbers = carsNumbers;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
