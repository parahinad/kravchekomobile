package ua.nure.dtpcontrol.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("numberPasport")
    @Expose
    private String numberPasport;
    @SerializedName("identificationCode")
    @Expose
    private String identificationCode;
    @SerializedName("balance")
    @Expose
    private Integer balance;

    public String getFullName() {
        return fullName;
    }

    public User setFullName(String fullName) {
        this.fullName = fullName;
        return mInstance;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public User setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return mInstance;
    }

    public String getNumberPasport() {
        return numberPasport;
    }

    public User setNumberPasport(String numberPasport) {
        this.numberPasport = numberPasport;
        return mInstance;
    }

    public String getIdentificationCode() {
        return identificationCode;
    }

    public User setIdentificationCode(String identificationCode) {
        this.identificationCode = identificationCode;
        return mInstance;
    }

    public Integer getBalance() {
        return balance;
    }

    public User setBalance(Integer balance) {
        this.balance = balance;
        return mInstance;
    }

    private static User mInstance;

    private User() {}

    public static User getInstance() {
        if(mInstance == null) {
            mInstance = new User();
        }
        return mInstance;
    }

    public String getId() {
        return id;
    }

    public User setId(String id) {
        this.id = id;
        return mInstance;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return mInstance;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return mInstance;
    }

    public String getToken() {
        return token;
    }

    public User setToken(String token) {
        this.token = token;
        return mInstance;
    }

}