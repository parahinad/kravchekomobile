package ua.nure.dtpcontrol.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Accidents {

    @SerializedName("accidents")
    @Expose
    private List<Accident> accidents = new ArrayList<Accident>();

    public List<Accident> getAccidents() {
        return accidents;
    }

    public void setAccidents(List<Accident> accidents) {
        this.accidents = accidents;
    }

}
